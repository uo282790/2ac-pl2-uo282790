#include <stdio.h>
#define NROWS	128
#define NCOLS	128
#define NTIMES	10000

//Matrix size
char matrix[NROWS][NCOLS];
int main(void){
	int i, j, rep;

	for(rep = 0; rep < NTIMES; rep++)
	{
		for(j = 0; j < NCOLS; j++)
		{
			for (i = 0; i < NROWS; i++)
			{
				matrix[i][j] = 'A';
				if (rep == 0 && i < 4)
				{
					if (j==0)
					{
						printf("Begin row\n");
					}
					printf("%p\n", &matrix[i][j]);
				}
			}
		}
	}
}
