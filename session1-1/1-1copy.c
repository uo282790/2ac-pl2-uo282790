#include <stdio.h>
#include <string.h>

int copy(char * source, char * destination, unsigned int lengthDestination)
{
	for (unsigned int i=0; i<lengthDestination; i++){
		*(destination + i) = *(source + i);
	}
	return 0;
}

int main()
{
	char * txt = "Hello";
	char * dest;
	unsigned int size = strlen(txt);

	printf("Size: %d\n", size);
	int res = copy(txt, dest, size);
	printf("Copied string: %s\n", dest);
	return 0;
}
