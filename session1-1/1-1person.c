#include <stdio.h>
#include <string.h>

//Objects in C
struct _Person
{
	char name[30];
	int heightcm;
	double weightkg;
};

typedef struct _Person Person;

int main (int argc, char* argv[])
{
	Person Peter;
	strcpy(Peter.name, "Peter");
	Peter.heightcm = 175;
	Peter.weightkg = 75.0;
	printf("Peter's height: %d, Peter's weight: %f\n", Peter.heightcm, Peter.weightkg);

	return 0;
}
