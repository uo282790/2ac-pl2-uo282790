#include <stdio.h>
#include <string.h>

//Objects in C
struct _Person
{
	char name[30];
	int heightcm;
	double weightkg;
};

typedef struct _Person Person;
typedef Person* PPerson;

int main (int argc, char* argv[])
{
	Person Javier;
	PPerson pJavier;
	pJavier = &Javier;

	strcpy(Javier.name, "Javier");
	Javier.heightcm = 180;
	Javier.weightkg = 84.0;
	pJavier->weightkg = 83.2;
	printf("Javier's height: %d, Javier's weight: %f\n", Javier.heightcm, Javier.weightkg);

	return 0;
}
