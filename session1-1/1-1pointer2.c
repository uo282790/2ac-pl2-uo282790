#include <stdio.h>

//Input is a pointer to a double
void f (double * d)
{
	//Edit the content of the memory, not the pointer
	*d = 4;
}

int main (int argc, char* argv[])
{
	double d;
	d = 3;

	//Use the memory address of d
	f(&d);
	printf("%f\n", d);
	return 0;
}
