#include <stdio.h>

#define NUM_ELEMENTS 7
	//Expects an array of ints and its length, i.e. its first position's memory address
int add(int * vectorStart, int length)
{
	int ret = 0;
	for (int i=0;i<length;i++){
			// Adds whatever is stored in the memory address of the array
		ret = ret + *(vectorStart+i);
	}
	return ret;
}

int main()
{
	int vector[NUM_ELEMENTS] = {2, 5, -2, 9, 12, -4, 3};
	int result;
	//Vector actually provides its start address
	result = add(vector, NUM_ELEMENTS);
	printf("The addition is: %d\n", result);
	return 0;
}
